from django.urls import path, include
from .views import article_list, article_detail, ArticleAPIView, ArticleDetails, GenericAPIViews, ArticleViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('article', ArticleViewSet, basename='article')

urlpatterns = [
    
    path('viewset/', include(router.urls)),
    # path('articles/', article_list),
    path('articles/', ArticleAPIView.as_view()),
    path('articles/<int:id>/', ArticleDetails.as_view()),
    path('generic/article/', GenericAPIViews.as_view()),
    path('generic/article/<int:id>/', GenericAPIViews.as_view()),
]
